package com.example;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

/**
 * This class contains simple test cases.
 *
 *  @author sah_anna
 */
public class SortingTest {

    Sorting sorting = new Sorting();

    @Test(expected = IllegalArgumentException.class)
    public void nullInputTest() {
        sorting.sort(null);
    }

    @Test
    public void emptyInputTest() {
        assertEquals("", sorting.sort(""));
    }

    @Test
    public void oneIntegerElementInputTest() {
        assertEquals("-100", sorting.sort("-100"));
    }

    @Test(expected = IllegalArgumentException.class)
    public void oneNonIntegerElementInputTest() {
        sorting.sort("csv");
    }

    @Test(expected = IllegalArgumentException.class)
    public void firstNonIntegerElementTest() {
        sorting.sort("abc 0 1 2 3");
    }

    @Test(expected = IllegalArgumentException.class)
    public void notFirstNonIntegerElementTest() {
        sorting.sort("0 1 abc 2 3");
    }

    @Test(expected = IllegalArgumentException.class)
    public void lastNonIntegerElementTest() {
        sorting.sort("0 1 2 3 4 5 6 7 8 abc");
    }

    @Test
    public void afterLastNonIntegerElementTest() {
        assertEquals("0 1 2 3 4 5 6 7 8 9", sorting.sort("0 1 2 3 4 5 6 7 8 9 abc"));
    }
}

package com.example;

import junitparams.FileParameters;
import junitparams.JUnitParamsRunner;

import org.junit.Test;
import org.junit.runner.RunWith;

import static org.junit.Assert.assertEquals;

/**
 * This class contains test cases parameterized by source file data.
 *
 *  @author sah_anna
 */
@RunWith(JUnitParamsRunner.class)
public class SortingFileParamTest {

    Sorting sorting = new Sorting();

    @Test
    @FileParameters("src/test/resources/input.csv")
    public void cornerCasesTest(String input, String expected) {
        assertEquals(expected, sorting.sort(input));
    }
}

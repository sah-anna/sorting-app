package com.example;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

import java.util.Arrays;
import java.util.Collection;

import static org.junit.Assert.assertEquals;

/**
 * This class contains test cases parameterized by internal data.
 *
 *  @author sah_anna
 */
@RunWith(Parameterized.class)
public class SortingParamTest {

    private String input;
    private String expected;
    Sorting sorting = new Sorting();

    public SortingParamTest(String input, String expected) {
        this.input = input;
        this.expected = expected;
    }

    @Parameters
    public static Collection<Object[]> data() {
        return Arrays.asList(new Object[][] {
                // zero arguments
                { "", "" },
                // one argument
                { "0", "0" },
                { "-1", "-1" },
                { "24", "24" },
                // ten arguments
                { "-32 -16 -8 -4 -2 -1 0 1 2 4", "-32 -16 -8 -4 -2 -1 0 1 2 4" },
                { "-16 -8 -2 4 2 -1 -4 0 -32 1", "-32 -16 -8 -4 -2 -1 0 1 2 4" },
                { "4 2 1 0 -1 -2 -4 -8 -16 -32", "-32 -16 -8 -4 -2 -1 0 1 2 4" },
                // more than ten arguments
                { "-32 -16 -8 -4 -2 -1 0 1 2 4 8 16 32", "-32 -16 -8 -4 -2 -1 0 1 2 4" },
                { "-32 -16 -8 -4 -2 -1 0 1 2 4 0.25", "-32 -16 -8 -4 -2 -1 0 1 2 4" },
                { "-32 -16 -8 -4 -2 -1 0 1 2 4 %%%% bla bla drop table", "-32 -16 -8 -4 -2 -1 0 1 2 4" }
        });
    }

    @Test
    public void cornerCasesTest() {
        assertEquals(expected, sorting.sort(input));
    }

}

/*
 * Simple application for demonstration of `maven` functionality.
 *
 */

package com.example;

import org.apache.log4j.Logger;


public class Main {

    static Logger logger = Logger.getLogger("SortingApp");

    public static void main(String[] args) {

        logger.info("Start app");

        String input = String.join(" ", args);
        logger.info(" Input: " + input);

        Sorting s = new Sorting();
        try {
            String result = s.sort(input);
            logger.info("Output: " + result);
            System.out.println(result);
        } catch (Exception e) {
            String message = e.getClass().getSimpleName() + ": " + e.getMessage();
            logger.error(message);
            System.out.println(message);
        }

        logger.info("Finish app");
    }
}

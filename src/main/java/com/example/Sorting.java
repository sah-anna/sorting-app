package com.example;

import java.util.Arrays;

/**
 * This class contains methods for sorting.
 *
 *  @author sah_anna
 */
public class Sorting {

    /**
     * Sorts input values in ascending order.
     * Input string may contain up to 10 integer values.
     * All next values starting form 11th will be ignored.
     *
     * @param  input  string of integer values separated by spaces
     * @throws IllegalArgumentException if meets non-integer value or input string is null
     * @return        string of sorted values separated by spaces
     */
    public String sort(String input) {

        if (input == null) {
            throw new IllegalArgumentException("input is null");
        } else if (input.isEmpty()) {
            return "";
        }

        String[] strArray = input.split(" ");
        int size = Math.min(strArray.length, 10);

        int[] intArray = new int[size];

        for (int i = 0; i < size; i++) {
            try {
                intArray[i] = Integer.parseInt(strArray[i]);
            } catch (NumberFormatException e) {
                throw new IllegalArgumentException("argument " + (i+1) + " is not an integer: " + strArray[i], e);
            }
        }

        if (size == 1) {
            return input;
        }

        Arrays.sort(intArray);

        StringBuilder result = new StringBuilder("" + intArray[0]);
        for (int i = 1; i < size; i++) {
            result.append(" ").append(intArray[i]);
        }

        return result.toString();
    }
}
